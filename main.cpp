﻿/*
Members:
Emory Wynn – 100655604 – emory.wynn@uoit.net
Joseph Di Lisi – 100590205 – joseph.dilisi@uoit.net
Luke Blanchette – 100660409 – Luke.Blanchette@uoit.net
Gabrielle Brown – 100655361 – Gabrielle.Brown@uoit.net
Lee Benzacar – 100665924 – Lmbenzacar@gmail.com
*/
//includes
#include "EmConsole.h"
#include "keyinput.h"
#include "Sound.h"
#include <string>
#include <chrono>
#include <thread>
#include <iostream>
#include "Minigames.h"
#include "BrickBreaker.h"
#include "tetris.h"

#define pauseThread(x) this_thread::sleep_for(chrono::milliseconds(x))

using namespace std;

const int cleantime = 60 * 5, feedtime = cleantime*1.5, affecttime = cleantime, 
deadtime = 1, playBBtime = 120, playTettime = 120;
keyinput key;
Console con("Textagucci Machine (8->)");
vector<vector<string>> tamaBg, tama, button,
gameOver = con.textFileToVector("game over.txt");
Sound  beep("beep.wav");
Tetris TET(con);
BrickBreaker BB(con);
Minigames *mgs[] = {&TET,&BB};
fstream save;

float vertical = 22, horizontal = 25;
int moveing = 1, choose = 0, buttonCount = 0, affection = 1;
bool direc = 0, clean = 1, full = 1, dead = 0, playGm[2];
time_t cleanT, feedT, affectT, deadT, gameT[2];

void youDiedAni()
{
	int a = 0;
	do
	{
		if(a == 0)
			while(a < 12)
			{
				con.toConsoleBuffer(gameOver[1], 22, 15 + a++);
				con.toConsoleBufferNS(tamaBg[0], 0, 0, con.FG_CYAN | con.FG_INTENSIFY);
				con.drawConsole();
				pauseThread(100);
			}
	} while(!key.stroke(VK_RETURN));

}


/*Tamagucci Stuff*/
int gameCount = 0;

void resetTime(time_t &t)
{
	time(&t);
}

void timeCount()
{
	//con.toConsoleBuffer(to_string(time(0)),0,0);
	//cleaning timer
	if(difftime(time(NULL), cleanT) >= cleantime)
	{
		clean = false;
	}
	if(difftime(time(NULL), feedT) >= feedtime)
	{
		full = false;
	}
	if(difftime(time(NULL), affectT) >= affecttime)
	{
		if(affection > 0)
			if(!full)
				affection--;

		if(affection > 0)
			if(!clean)
				affection--;

		resetTime(affectT);
	}
	if(difftime(time(NULL), deadT) / 60 / 60 / 24 >= deadtime)
	{
		dead = 1;
	}

	if(difftime(time(NULL), gameT[0]) >= playTettime)
	{
		playGm[0] = false;
	}
	if(difftime(time(NULL), gameT[1]) >= playBBtime)
	{
		playGm[1] = false;
	}
	
}

void resetAllTime()
{
	time(&cleanT);
	time(&feedT);
	time(&affectT);
	time(&gameT[0]);
	time(&gameT[1]);
	time(&deadT);
}

bool chooseGame()
{
	vector<vector<string>> gt = con.textFileToVector("Game Titles.txt");
	while(true)
	{
		if(key.stroke(VK_LEFT))
			gameCount - 1 >= 0 ? gameCount-- : gameCount = gt.size() - 1;
		else if(key.stroke(VK_RIGHT))
			gameCount + 1 < 2 ? gameCount++ : gameCount = 0;
		con.toConsoleBuffer(tamaBg[0], 0, 0, con.FG_CYAN | con.FG_INTENSIFY);

		con.toConsoleBuffer(gt[gameCount], (59 / 2 - gt[gameCount][0].size() / 2) + 19 / 2, (42 / 2 - gt[gameCount].size() / 2) + 15 / 2);

		if(key.stroke('Q'))
			return 0;

		int min, sec;

		if(playGm[gameCount])
		{
			min = difftime(time(0), gameT[gameCount]) / 60;
			sec = difftime(time(0), gameT[gameCount]) - min * 60;
			min -= 1, sec -= 60;
			min *= -1, sec *= -1;
			con.toConsoleBuffer(to_string(min) + " : " + to_string(sec), 19, 15);

		}

		if(key.stroke(VK_RETURN) && !playGm[gameCount])
			break;
		con.drawConsole();
		timeCount();
	}
	return 1;
}

void games()
{
	if(chooseGame())
	{

		if(gameCount == 0)
		{
			mgs[0]->play();
			playGm[0] = 1;
			resetTime(gameT[0]);
		} else if(gameCount == 1)
		{
			mgs[1]->play();
			playGm[1] = 1;
			resetTime(gameT[1]);
		}
		resetTime(affectT);
		affection++;
	}
}

void movement()
{
	if(key.stroke(VK_RIGHT))
	{
		//horizontal += moveing * 2;
		buttonCount++;
		direc = 0;
		beep.play();
	} else if(key.stroke(VK_LEFT))
	{
		//horizontal -= moveing * 2;
		buttonCount--;
		direc = 1;
		beep.play();
	} else

		if(buttonCount == 0 && key.stroke(VK_RETURN))
		{
			clean = 1;
		} else if(buttonCount == 1 && key.stroke(VK_RETURN))
		{
			games();
		} else if(clean && buttonCount == 2 && key.stroke(VK_RETURN))
		{
			resetTime(cleanT);
			resetTime(feedT);
			full = 1;
		}

		if(!clean)
			resetTime(cleanT);
		if(!full)
		{
			resetTime(feedT);
			resetTime(cleanT);
		}

		if(!playGm[0])
			resetTime(gameT[0]);
		if(!playGm[1])
			resetTime(gameT[1]);

		if(clean&&full)
			resetTime(affectT);

		if(affection > 0)
			resetTime(deadT);

		if(buttonCount < 0)
			buttonCount = 2;
		else if(buttonCount >= 3)
			buttonCount = 0;
}

void choice()
{
	if(key.stroke(VK_LEFT))
		choose -= 2;
	else if(key.stroke(VK_RIGHT))
		choose += 2;

	if(choose < 0)
		choose = tama.size() - 2;
	else if(choose >= tama.size())
		choose = 0;
}

void chooseTamagatchi()
{
	con.clearConsole();
	do
	{
		choice();
		con.toConsoleBuffer(tamaBg[0], 0, 0, con.FG_CYAN | con.FG_INTENSIFY);
		con.toConsoleBuffer("Choose Your Tamagatchi (<- ->)", 19 + 5, 15);
		con.toConsoleBuffer(tama[choose], horizontal = (19 + 20) - tama[choose][0].size() / 2, vertical = (28) - tama[choose].size() / 2);
		con.drawConsole();
	} while(!key.stroke(VK_RETURN));
	//OutputDebugString("It got here\n");
}


//27, 40
void turnOn()
{
	//con.setConsoleSize(tamaBg[0][0].size(), tamaBg[0].size());
	//con.toConsoleBuffer(tamaBg[0], 0, 0);
	//con.drawConsole();
	for(int a = 0; a < 14; a++)
	{
		con.toConsoleBuffer(tamaBg[0], 0, 0, con.FG_CYAN);
		con.toConsoleBuffer("                                        ", 19, 14 + 14, con.BG_WHITE);
		for(int b = 0; b < a; b++)
		{
			con.toConsoleBuffer("                                        ", 19, 14 + 14 - b - 1, con.BG_WHITE);
			con.toConsoleBuffer("                                        ", 19, 14 + 14 + b + 1, con.BG_WHITE);
		}
		con.drawConsole();
		pauseThread(50);
	}
	if(!ifstream("save.dat"))
		chooseTamagatchi();
}
void turnOff()
{
	con.setConsoleSize(tamaBg[0][0].size(), tamaBg[0].size());
	for(int a = 0; a < 15; a++)
	{
		con.toConsoleBuffer(tamaBg[0], 0, 0, con.FG_CYAN);
		if(a < 14)
			con.toConsoleBuffer("                                        ", 19, 14 + 14, con.BG_WHITE);

		for(int b = 0; b < 13 - a; b++)
		{
			con.toConsoleBuffer("                                        ", 19, 14 + 14 - b - 1, con.BG_WHITE);
			con.toConsoleBuffer("                                        ", 19, 14 + 14 + b + 1, con.BG_WHITE);
		}
		con.drawConsole();
		pauseThread(50);
	}
}

void buttonPosition(COORD pos)
{
	con.toConsoleBuffer(button[0][0], pos.X + 3, pos.Y++, con.BG_BLUE);
	con.toConsoleBuffer(button[0][1], pos.X + 2, pos.Y++, con.BG_BLUE);
	con.toConsoleBuffer(button[0][2], pos.X + 1, pos.Y++, con.BG_BLUE);
	con.toConsoleBuffer(button[0][3], pos.X + 1, pos.Y++, con.BG_BLUE);
	con.toConsoleBuffer(button[0][4], pos.X + 2, pos.Y++, con.BG_BLUE);
	con.toConsoleBuffer(button[0][5], pos.X + 3, pos.Y++, con.BG_BLUE);
}

void saveData()
{
	save.close();
	save.open("save.dat", ios::out | ios::binary);
	//time_t t = time(0);
	//tm* time = 0;
	//localtime_s(time,&t);
	//save << time<<endl;
	save << choose << endl;
	save << affection << endl;
	save << full << endl;
	save << clean << endl;
	save << horizontal << endl;
	save << vertical << endl;
	save << affectT << endl;
	save << feedT << endl;
	save << cleanT << endl;
	save << deadT << endl;
	save << gameT[0] << endl;
	save << gameT[1] << endl;
	save << playGm[0] << endl;
	save << playGm[1] << endl;
	save.close();
}

void readData()
{
	save.open("save.dat", ios::in | ios::binary);
	save >> choose;
	save >> affection;
	save >> full;
	save >> clean;
	save >> horizontal;
	save >> vertical;
	save >> affectT;
	save >> feedT;
	save >> cleanT;
	save >> deadT;
	save >> gameT[0];
	save >> gameT[1];
	save >> playGm[0];
	save >> playGm[1];
	save.close();

	double difffeed = difftime(time(0), feedT), diffclean = difftime(time(0), cleanT);

	if(affection > 0)
		if(difffeed / (feedtime) + diffclean / (cleantime) >= affection)
		{
			affectT += (diffclean > difffeed ? cleanT : feedT) - affectT;
			for(int a = 1; affection - a > 0; a++)
			{
				if(diffclean > difffeed)
					affectT += (a % 2 != 0 ? cleantime : feedtime);

				else
					affectT += (a % 2 == 0 ?cleantime : feedtime);

			}
		}
	timeCount();
}

void main()
{

	tama = con.textFileToVector("Tamagatchi.txt");
	tamaBg = con.textFileToVector("tama bg.txt");
	button = con.textFileToVector("button.txt");
	con.setConsoleSize(tamaBg[0][0].size(), tamaBg[0].size());
	con.setConsolePosition(0, 0);
	vector<COORD> butonLocal;
	butonLocal.push_back({19,45});
	butonLocal.push_back({34,48});
	butonLocal.push_back({50,45});
	turnOn();

	if(fstream("save.dat"))
		readData();
	else
		resetAllTime();

	do
	{
		con.toConsoleBuffer(tamaBg[0], 0, 0, con.FG_CYAN | con.FG_INTENSIFY);
		movement();
		timeCount();
		con.toConsoleBuffer(tama[choose + direc], horizontal, vertical);
		con.toConsoleBuffer("  Clean   ", 19, 43);
		con.toConsoleBuffer("   Play   ", 34, 46);
		con.toConsoleBuffer("   Feed   ", 50, 43);
		con.toConsoleBuffer(vector<string>{"Affection", "    " + (affection < 3 ? to_string(affection) : "max")}, 19, 15);
		con.toConsoleBuffer(vector<string>{"  Hunger", (!full ? "need food" : "   full")}, 33, 15);
		con.toConsoleBuffer(vector<string>{"   Cleaning", (clean ? "  all clean" : "need cleaning")}, 46, 15);
		con.toConsoleBuffer("Q - Turn Off Textagatchi", 59-25, 41);
		buttonPosition(butonLocal[buttonCount]);
		
		if(dead)
			youDiedAni();

		if(!clean)
		{
			vector<string> poo =
			{
			"     (   )",
			"   (  ) (",
			"    ) _  )",
			"     ( \\_",
			"   _(_\\ \\)__",
			"  (____\\___))"
			};
			con.toConsoleBufferNS(poo, 18, 36);
		}

		con.drawConsole();
		if(affection > 3)
			affection = 3;

	} while(!key.stroke('Q') && !dead);

	if(!dead)
		saveData();
	else
		remove("save.dat");

	turnOff();
}