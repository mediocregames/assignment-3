#pragma once
#include "Minigames.h"
#include "EmConsole.h"
#include "keyinput.h"
#include "Sound.h"
#include <string>
#include <chrono>
#include <thread>
#include<vector>
#include <iostream>


using namespace std;
#define pauseThread(x) this_thread::sleep_for(chrono::milliseconds(x))


class Tetris:public Minigames
{
public:

	void play()
	{
		tetris();
	}

	Tetris(Console &con):con(con), tempBoardAtra(26, vector<int>(40, 11)), tetrisBGM("tetris remix.wav")
	{
		
	}

	void gameOverAni()
	{
		int a = 0;
		do
		{
			if(a == 0)
				while(a < 12)
				{
					con.toConsoleBuffer(gameOver[0], 18, 15 + a++);
					con.toConsoleBufferNS(tamaBg[0], 0, 0, con.FG_CYAN | con.FG_INTENSIFY);
					if(a==12)
					con.toConsoleBuffer("Press \"Enter\" to continue...", 59 - 29, 41);
					con.drawConsole();
					pauseThread(100);
				}
		} while(!key.stroke(VK_RETURN));

	}

	/*Tetris stuff*/
	int highScoreTet, scoreTet = 0, tetEnd = 0,
		x = 20, y = 16, guideX = x, guideY = 40, rotation = 0,
		downMoveCount, horizontalMoveCount, verticalMoveCount, colourNum, gameChoice;
	bool colisionFullTetris(int x, int y, vector<string> &shape)
	{
		for(int a = shape.size() - 1; a >= 0; a--)
		{
			for(int b = 0; b < shape[a].size(); b++)
			{
				if(shape[a][b] != ' ')
					if(con.readConsoleCharacter(x + b, y + a) != ' ')
						return 1;
			}
		}
		return 0;
	}

	bool colisionLeftTetris(int x, int y, vector<string> &shape)
	{

		for(int a = shape.size() - 1; a >= 0; a--)
		{		
					if(con.readConsoleCharacter(x, y + a) != ' ')
						return 1;		
		}
		return 0;
	}

	bool colisionRightTetris(int x, int y, vector<string> &shape)
	{
		for(int a = shape.size() - 1; a >= 0; a--)
		{
			
					if(con.readConsoleCharacter(x + shape[a].size() - 1, y + a) != ' ')
						return 1;
			
			
		}
		return 0;
	}

	bool colisionTopTetris(int x, int y, vector<string> &shape)
	{
		int b = 0;
		for(int a = 0; a < shape.size(); a++)
		{
			while(b < shape[a].size())
			{
				if(shape[a][b] != ' ')
				{
					if(con.readConsoleCharacter(x + b, y + a) != ' ')
						return 1;
					else
					{
						b++;
						a = -1;
						break;
					}
				} else
				{
					if(a == shape.size() - 1)
					{
						b++;
						a = -1;
					}

					break;
				}
			}
		}
		return 0;
	}

	bool colisionBottomTetris(int x, int y, vector<string> &shape)
	{
		int  b = 0;
		for(int a = shape.size() - 1; a >= 0; a--)
		{
			while(b < shape[a].size())
			{
				if(shape[a][b] != ' ')
				{
					if(con.readConsoleCharacter(x + b, y + a) != ' ')
						return 1;
					else
					{
						b++;
						a = shape.size();
						break;
					}
				} else
				{
					if(a == 0)
					{
						b++;
						a = shape.size();

					}
					break;
				}
			}
		}

		return 0;
	}

	void deleteLines()
	{
		int sc = 0;
		for(int a = 40; a > 16; a--)
		{
			if((con.readConsoleLine(20, a, 38)) == "**************************************")
			{
				con.toConsoleBuffer(con.readConsoleArea(20, 16, 38, a - 16), 20, 17, con.readConsoleAreaAtributes(20, 16, 38, a - 16));
				sc++;
				a++;
			}
		}
		scoreTet += sc*sc * 100;

		if(scoreTet > highScoreTet)
		{
			highScoreTet = scoreTet;
			save.open("high score tet.dat", ios::out | ios::binary);
			save << highScoreTet << endl;
			save.close();
		}
	}

	void restartTetris()
	{
		shapes.clear();
		tempBoardAtra.clear();

		shapes = shapeHolder[colourNum = (rand() % shapeHolder.size())];
		x = 20;
		y = 16;
		rotation = 0;
		tempBoardAtra.assign(26, vector<int>(40, 11));

		tempBoard.open("tempBoard.txt", fstream::out);
		tempBoard << "";
		//tempBoard.flush();
		//tempBoard.clear();
		tempBoard.close();
		scoreTet = 0;
	}

	void tetrisShapeMovement()
	{
		bool check = 0;

		if(horizontalMoveCount++ > 3)
		{
			horizontalMoveCount = 0;
			if(key.press(VK_LEFT) && !colisionFullTetris(x - 2, y, shapes[rotation]))
				x -= 2;
			else if(key.press(VK_RIGHT) && !colisionFullTetris(x + 1, y, shapes[rotation]))
				x += 2;

		}

		if(verticalMoveCount++ > 1)
		{
			verticalMoveCount = 0;
			if(key.press(VK_DOWN))
			{
				y += 2;
				check = 1;
			}
		}

		if(key.stroke('A'))
		{
			if(rotation > 0)
			{
				if(!colisionFullTetris(x, y, shapes[rotation - 1]))
					rotation--;
			} else
				if(!colisionFullTetris(x, y, shapes[shapes.size() - 1]))
					rotation = shapes.size() - 1;
		} else if(key.stroke('D'))
			if(rotation < shapes.size() - 1)
			{
				if(!colisionFullTetris(x, y, shapes[rotation + 1]))
					rotation++;
			} else
				if(!colisionFullTetris(x, y, shapes[0]))
					rotation = 0;



			if(!key.press(VK_DOWN) && downMoveCount++ == 10)
			{
				y++;
				downMoveCount = 0;
				check = 1;
			}

			if(check && colisionFullTetris(x, y, shapes[rotation]))
			{

				while(colisionFullTetris(x, y, shapes[rotation]))
					y--;

				con.toConsoleBufferNS(shapes[rotation], x, y, colour[colourNum]);
				deleteLines();
				tempBoard.open("tempBoard.txt", fstream::out);

				for(int a = 15; a <= 40; a++)
					tempBoard << con.readConsoleLine(19, a, 40) << endl;
				tempBoard.close();
				tempBoardAtra = con.readConsoleAreaAtributes(19, 15, 40, 26);

				y = 16;
				shapes = shapeHolder[colourNum = (rand() % shapeHolder.size())];
				rotation = rand() % shapes.size();


				if((colisionFullTetris(20, 19, vector<string>{ "**************************************"})))
				{
					OutputDebugStringA("game has restarted!!\n");
					gameOverAni();
					tetEnd = true;
					return;
				}
				if(colisionFullTetris(x, y, shapes[rotation]))
				{
					if(colisionLeftTetris(x, y, shapes[rotation]))
						while(colisionLeftTetris(++x, y, shapes[rotation]));
					else
						while(colisionFullTetris(--x, y, shapes[rotation]));

				}


			}
	}

	void tetrisGuideMovement()
	{
		for(guideY = y; !colisionFullTetris(x, guideY, shapes[rotation]); guideY++);
		guideY--;
	}

	void tetrisControles()
	{
		vector<string>controle {"Controles: ","","Left/Right - Move Block","","Down - Accelerate","","A/D - Rotate","","R - Reset","","Q - Exit Game"};
		while(!key.stroke(VK_RETURN))
		{
			con.toConsoleBuffer(tamaBg[0], 0, 0, con.FG_CYAN | con.FG_INTENSIFY);
			con.toConsoleBuffer(controle, 30, 23);
			con.drawConsole();
		}
	}

	void tetris()
	{
		srand(time(NULL));
		tempBoard.close();
		board = con.textFileToVector("Tetris Board.txt");

		shapeHolder.push_back(con.textFileToVector("shape1.txt"));
		shapeHolder.push_back(con.textFileToVector("shape2.txt"));
		shapeHolder.push_back(con.textFileToVector("shape3.txt"));
		shapeHolder.push_back(con.textFileToVector("shape4.txt"));

		colour.push_back(con.FG_RED | con.FG_INTENSIFY);
		colour.push_back(con.FG_PURPLE | con.FG_INTENSIFY);
		colour.push_back(con.FG_YELLOW | con.FG_INTENSIFY);
		colour.push_back(con.FG_GREEN | con.FG_INTENSIFY);

		tetrisControles();
		tetrisBGM.play(true);

		tetEnd = 0;
		restartTetris();
		if(ifstream("high score tet.dat"))
		{
			save.open("high score tet.dat", ios::in | ios::binary);
			save >> highScoreTet;
			save.close();
		}
		while(!key.stroke('Q') && !tetEnd)
		{
			con.toConsoleBuffer(tamaBg[0], 0, 0, con.FG_CYAN | con.FG_INTENSIFY);
			con.toConsoleBuffer(board[0], 19, 15, con.FG_CYAN | con.FG_INTENSIFY);
			con.toConsoleBuffer(con.textFileToVector("tempBoard.txt")[0], 19, 15, tempBoardAtra);

			tetrisShapeMovement();
			tetrisGuideMovement();
			con.toConsoleBuffer("**************************************", 20, 19, con.FG_RED);
			con.toConsoleBufferNS(shapes[rotation], x, guideY);
			con.toConsoleBufferNS(shapes[rotation], x, y, colour[colourNum]);
			con.toConsoleBuffer("Score", 19 + 40 - 6, 16);
			con.toConsoleBuffer(to_string(scoreTet), 19 + 39 - to_string(scoreTet).size(), 17);
			con.toConsoleBuffer("High Score", 20, 16);
			con.toConsoleBuffer(to_string(highScoreTet), 20, 17);

			if(key.stroke('R'))
				restartTetris();
			if(!tetEnd)
				con.drawConsole();
			pauseThread(10);
		}
		if(key.stroke('Q'))
			NULL;
		tetrisBGM.stopSound();
		
		colour.clear();
		shapeHolder.clear();


	}

private:
	/*tetris variables*/
	Console con;
	vector<vector<vector<string>>> shapeHolder;
	vector<vector<string>>shapes, board;
	vector<vector<int>>tempBoardAtra;
	vector <int>  colour;
	fstream tempBoard,save;
	Sound tetrisBGM;
	keyinput key;
	vector<vector<string>> tamaBg = con.textFileToVector("tama bg.txt"),
		gameOver = con.textFileToVector("game over.txt");
};